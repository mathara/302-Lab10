package base.cartas.magia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

import base.cartas.Carta;
import base.cartas.Lacaio;
import base.service.ILaMaSerializable;
import io.Escritor;

public class DanoArea extends Dano{

	public DanoArea(UUID id,String nome, int custoMana, int dano) {
		super(id, nome, custoMana, dano);
	}
	
	public DanoArea(String nome, int custoMana, int dano) {
		super(nome, custoMana, dano);
	}
	
	@Override
	public String toString(){
		String out = super.toString();
		return out;
	}
	
	@Override
	public void usar(ArrayList<Carta> alvos){
		/*int j = alvos.size();
		int i = 0;
		
		while (i < j){	
		Lacaio aux = (Lacaio) alvos.get(i);
		aux.setVidaAtual(aux.getVidaAtual() - this.getDano());
		alvos.add(i,aux);
		alvos.remove(i+1);
		i++;
		}*/
		for(int i = 0; i <alvos.size();i++){
			super.usar(alvos.get(i));
		}
		
	}
	
	@Override
	public void escreveAtributos(Escritor fw) throws IOException {
		// TODO Auto-generated method stub
		fw.escreveDelimObj("DanoArea");
		fw.escreveAtributo("id", getId().toString());
		fw.escreveAtributo("nome", getNome());
		fw.escreveAtributo("custo_mana",String.valueOf(getCustoMana()));
		fw.escreveAtributo("dano", String.valueOf(getDano()));
		fw.escreveDelimObj("DanoArea");
	}

	@Override
	public ILaMaSerializable fromFile(Scanner inputaux) throws IOException {
		String token;
		
		token = inputaux.next();
		assert token.equals("id");
		UUID id = UUID.fromString(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("nome");
		String nome = inputaux.next();
		
		token = inputaux.next();
		assert token.equals("custoMana");
		int custoM = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("dano");
		int dano = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("obj");
		token = inputaux.next();
		
		return new DanoArea(id, nome, custoM, dano );
	}
	

}
