package base.cartas.magia;

import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;

import base.cartas.Carta;
import base.cartas.HabilidadesLacaio;
import base.cartas.Lacaio;
import base.service.ILaMaSerializable;
import io.Escritor;

public class Buff extends Magia{
	private int aumentoEmAtaque;
	private int aumentoEmVida;
	
	public Buff(UUID id, String nome, int custoMana, int aumentoEmAtaque, int aumentoEmVida) {
		super(id, nome, custoMana);
		this.aumentoEmAtaque = aumentoEmAtaque;
		this.aumentoEmVida = aumentoEmVida;
	}

	public Buff(String nome, int custoMana, int aumentoEmAtaque, int aumentoEmVida) {
		super(nome, custoMana);
		this.aumentoEmAtaque = aumentoEmAtaque;
		this.aumentoEmVida = aumentoEmVida;
	}
	
	public int getAumentoEmAtaque(){
		return aumentoEmAtaque;
	}
	
	public void setAumentoEmAtaque(int aumentoEmAtaque){
		this.aumentoEmAtaque = aumentoEmAtaque;
	}
	
	public int getAumentoEmVida(){
		return aumentoEmVida;
	}
	
	public void setAumentoEmVida(int aumentoEmVida){
		this.aumentoEmVida = aumentoEmVida;
	}
	
	@Override
	public void usar(Carta alvo){
		Lacaio aux = (Lacaio) alvo;
		aux.setAtaque(aux.getAtaque() + this.aumentoEmAtaque );
		aux.setVidaAtual(aux.getVidaAtual() + this.aumentoEmVida);
		
		//não foi especificado, se poderia ou não fazer isso
		if (aux.getVidaAtual() == aux.getVidaMaxima()){
			aux.setVidaMaxima(aux.getVidaMaxima() + this.aumentoEmVida);
		}
		
		alvo = aux;
	}
	
	@Override
	public String toString(){
		String out = super.toString();
		out += "Aumento em Ataque = " +getAumentoEmAtaque() + "\n";
		out += "Aumento em Vida = " +getAumentoEmVida() + "\n";
		return out;
	}

	@Override
	public void escreveAtributos(Escritor fw) throws IOException {
		// TODO Auto-generated method stub
		fw.escreveDelimObj("Buff");
		fw.escreveAtributo("id", getId().toString());
		fw.escreveAtributo("nome", getNome());
		fw.escreveAtributo("custo_mana",String.valueOf(getCustoMana()));
		fw.escreveAtributo("aumento em ataque", String.valueOf(getAumentoEmVida()));
		fw.escreveAtributo("aumento em vida", String.valueOf(getAumentoEmVida()));
		fw.escreveDelimObj("Buff");
	}

	@Override
	public ILaMaSerializable fromFile(Scanner inputaux) throws IOException {
		// TODO Auto-generated method stub
		
		String token;
		
		token = inputaux.next();
		assert token.equals("id");
		UUID id = UUID.fromString(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("nome");
		String nome = inputaux.next();
		
		token = inputaux.next();
		assert token.equals("custoMana");
		int custoM = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		//assert token.equals("ataque");
		int aumentoEmAtaque = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		//assert token.equals("vidaAtual");
		int aumentoEmVida = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("obj");
		token = inputaux.next();
		
		return new Buff(id, nome, custoM, aumentoEmAtaque, aumentoEmVida );
	}
	
}
