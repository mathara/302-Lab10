package base.cartas.magia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

import base.cartas.Carta;
import base.cartas.HabilidadesLacaio;
import base.cartas.Lacaio;
import base.service.ILaMaSerializable;
import io.Escritor;

public class Dano extends Magia{
	private int dano;
	
	public Dano(UUID id ,String nome, int custoMana, int dano) {
		super(id, nome, custoMana);
		this.dano = dano;
	}
	
	public Dano(String nome, int custoMana, int dano) {
		super(nome, custoMana);
		this.dano = dano;
	}
	
	public int getDano(){
		return dano;
	}
	
	public void setDano(int dano){
		this.dano = dano;
	}
	
	@Override
	public String toString(){
		String out = super.toString();
		out += "Dano = " +getDano() + "\n";
		return out;
	}
	
	@Override
	public void usar(Carta alvo){
		Lacaio aux = (Lacaio) alvo;
		aux.setVidaAtual(aux.getVidaAtual() - this.dano);
		
		alvo = aux;
	}

	@Override
	public void escreveAtributos(Escritor fw) throws IOException {
		// TODO Auto-generated method stub
		fw.escreveDelimObj("Dano");
		fw.escreveAtributo("id", getId().toString());
		fw.escreveAtributo("nome", getNome());
		fw.escreveAtributo("custo_mana",String.valueOf(getCustoMana()));
		fw.escreveAtributo("dano", String.valueOf(getDano()));
		fw.escreveDelimObj("Dano");
	}

	@Override
	public ILaMaSerializable fromFile(Scanner inputaux) throws IOException {
		String token;
		
		token = inputaux.next();
		assert token.equals("id");
		UUID id = UUID.fromString(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("nome");
		String nome = inputaux.next();
		
		token = inputaux.next();
		assert token.equals("custoMana");
		int custoM = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("dano");
		int dano = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("obj");
		token = inputaux.next();
		
		return new Dano(id, nome, custoM, dano );
	}
	
}
