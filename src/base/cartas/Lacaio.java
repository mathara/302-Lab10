package base.cartas;

import java.io.IOException;
import java.util.Formatter;
import java.util.Scanner;
import java.util.UUID;

import base.service.ILaMaSerializable;
import io.Escritor;
import io.Leitor;

public class Lacaio extends Carta{
	private int ataque;
	private int vidaAtual;
	private int vidaMaxima;
	private HabilidadesLacaio  habilidade;
	
	//metodo construtor 
	public Lacaio(UUID id, String nome, int custoMana, int ataque, int vidaAtual, int vidaMax){
		super(id, nome, custoMana);
		this.ataque = ataque;
		this.vidaAtual = vidaAtual;
		this.vidaMaxima = vidaMax;  
	}
	
	//construtor reduzido
	public Lacaio(String nome, int custoMana, int ataque, int vidaAtual, int vidaMax /*HabilidadeLacaio*/){
		super(nome, custoMana);
		this.ataque = ataque;
		this.vidaAtual = vidaAtual;
		this.vidaMaxima = vidaMax;  
	}
	
	public Lacaio(String nome, int custoMana, int ataque, int vidaAtual, int vidaMax, HabilidadesLacaio habilidade){
		super(nome, custoMana);
		this.ataque = ataque;
		this.vidaAtual = vidaAtual;
		this.vidaMaxima = vidaMax;
		this.habilidade = habilidade;
	}
	
	public Lacaio(UUID id, String nome, int custoMana, int ataque, int vidaAtual, int vidaMax, HabilidadesLacaio habilidade){
		super(id, nome, custoMana);
		this.ataque = ataque;
		this.vidaAtual = vidaAtual;
		this.vidaMaxima = vidaMax;
		this.habilidade = habilidade;
	}	
	
	
	//Demais metodos
	public int getAtaque(){
		return ataque;
	}
	
	public void setAtaque(int ataque){
		this.ataque = ataque;
	}
	
	public int getVidaAtual(){
		return vidaAtual;
	}
	
	public void setVidaAtual(int vidaAtual){
		this.vidaAtual = vidaAtual;
	}
	
	public int getVidaMaxima(){
		return vidaMaxima;
	}
	
	public void setVidaMaxima(int vidaMaxima){
		this.vidaMaxima = vidaMaxima;
	}
	
	public HabilidadesLacaio getHabilidade(){
		return habilidade;
	}
	
	public void setHabilidade (HabilidadesLacaio h){
		this.habilidade = h;
	}
	
	@Override
	//Alterar
	public String toString(){
		String out = super.toString();
		out += "Ataque = " +getAtaque() + "\n";
		out += "Vida Atual = " +getVidaAtual() + "\n";
		out += "Vida Maxima = " +getVidaMaxima() + "\n";
		return out;
	}
	
	@Override
	public void usar(Carta alvo){
		Lacaio aux = (Lacaio) alvo;
		aux.vidaAtual -= this.ataque;
		alvo = aux;
	}

	@Override
	public void escreveAtributos(Escritor fw) throws IOException {
		fw.escreveDelimObj("Lacaio");
		fw.escreveAtributo("id", getId().toString());
		fw.escreveAtributo("nome", getNome());
		fw.escreveAtributo("custo_mana",String.valueOf(getCustoMana()));
		fw.escreveAtributo("ataque", String.valueOf(getAtaque()));
		fw.escreveAtributo("vida_atual", String.valueOf(getVidaAtual()));
		fw.escreveAtributo("vida_max", String.valueOf(getVidaMaxima()));
		fw.escreveAtributo("estilo", getHabilidade().toString());
		fw.escreveDelimObj("Lacaio");
		
	}

	@Override
	public ILaMaSerializable fromFile(Scanner inputaux) throws IOException {
		String token;
		
		token = inputaux.next();
		assert token.equals("id");
		UUID id = UUID.fromString(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("nome");
		String nome = inputaux.next();
		
		token = inputaux.next();
		assert token.equals("custoMana");
		int custoM = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("ataque");
		int ataque = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("vidaAtual");
		int vidaA = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("vidaMaxima");
		int vidaM = Integer.valueOf(inputaux.next());
		
		token = inputaux.next();
		assert token.equals("habilidade");
		String thab =inputaux.toString();
		
		HabilidadesLacaio hab = null;
		if (thab.equals("EXAUSTAO") ){
			hab = HabilidadesLacaio.EXAUSTAO;
		}
		else if (thab.equals("PROVOCAR")){
			hab = HabilidadesLacaio.PROVOCAR;
		}
		else if (thab.equals("INVESTIDA")){
			hab = HabilidadesLacaio.INVESTIDA;
		}
		
		token = inputaux.next();
		assert token.equals("obj");
		token = inputaux.next();
		
		return new Lacaio(id, nome, custoM, ataque, vidaA, vidaM, hab );
	}
	
	

}
