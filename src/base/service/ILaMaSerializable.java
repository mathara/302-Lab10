package base.service;

import io.Escritor;

import java.io.IOException;
import java.util.Formatter;
import java.util.Scanner;

public interface ILaMaSerializable {
	void escreveAtributos (Escritor fw) throws IOException;
	ILaMaSerializable fromFile(Scanner inputaux) throws IOException;
}
