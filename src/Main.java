import java.io.File;
import java.io.IOException;
import java.util.List;

import base.cartas.*;
import base.cartas.magia.*;
import base.service.*;
import io.*;
import util.*;

public class Main {
	public static void main(String[] args) throws IOException  {
		//gerar lacaios, dano, danoArea e Magia
		Carta A = new Lacaio("AB", 1, 2, 3,4, HabilidadesLacaio.EXAUSTAO );
		Carta B = new Buff("Up", 7, 10 , 10);
		Carta C = new Dano ("Dano", 2, 5);
		Carta D = new DanoArea ("DanoArea", 3, 7);
		
		//escritor
		Escritor papel = new Escritor();
		
		//escreve no arquivo
		A.escreveAtributos(papel);
		B.escreveAtributos(papel);
		C.escreveAtributos(papel);
		D.escreveAtributos(papel);
		
		
		//leitura do papel
		try{
			Leitor leitura = new Leitor("Arquivo.txt");
			
			List<ILaMaSerializable> cards = null;
			cards = leitura.leObjeto();
			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
	}
}