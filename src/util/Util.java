package util;

import java.util.Random;

import base.cartas.Carta;
import base.cartas.HabilidadesLacaio;
import base.cartas.Lacaio;
import base.cartas.TipoCarta;
import base.cartas.magia.Buff;
import base.cartas.magia.Dano;
import base.cartas.magia.DanoArea;

public class Util {
	public static final int MAX_CARDS = 30;
	public static final int MAX_NOME = 5;
	public static final int MAO_INI = 3;
	public static final int MAX_TURNOS = 10;
	public static final int PODER_HEROI = 10;
	public static final int MANA_INI = 1;
	public static final int MAX_LACAIOS = 10;
	public static final int MAX_MANA = 2;
	public static final int MAX_ATAQUE = 6;
	public static final int MAX_VIDA = 6;
//	
//	public static void buffar(Lacaio lac,int a){
//		if (a > 0){
//			lac.setVidaAtual(lac.getVidaAtual() + a);
//			lac.setVidaMaxima(lac.getVidaMaxima() + a);
//			lac.setAtaque(lac.getAtaque() + a);
//			alteraNomeFortalecido(lac);
//		}
//	}
//	
//	public static void buffar(Lacaio lac, int a, int v){
//		if (a > 0 && v > 0){
//			lac.setVidaAtual(lac.getVidaAtual() + v);
//			lac.setVidaMaxima(lac.getVidaMaxima() + v);
//			lac.setAtaque(lac.getAtaque() + a);
//			alteraNomeFortalecido(lac);
//		}
//	}
//	
//	public static void alteraNomeFortalecido(Lacaio lac){
//		lac.setNome( lac.getNome()+ "Buffed");
//	}
//	
//	public static Carta geraCartaAleatoria(Random generator, int maxMana, int maxAtaque, 
//			int maxVida, TipoCarta tc){
//		Carta aux = null;
//		RandomString stringGerador = new RandomString(generator, MAX_NOME);
//		
//		if (tc == TipoCarta.BUFF){
//			aux = new Buff(stringGerador.nextString(), radInt(generator,1,maxMana), radInt(generator,1,maxAtaque), radInt(generator,1,maxVida));
//		}
//		else if (tc == TipoCarta.DANO){
//			aux = new Dano(stringGerador.nextString(), radInt(generator,1,maxMana), radInt(generator,1,maxAtaque));
//		}
//		else if (tc == TipoCarta.DANO_AREA){
//			aux = new DanoArea(stringGerador.nextString(), radInt(generator,1,maxMana), radInt(generator,1,maxAtaque));
//		}
//		else if (tc == TipoCarta.LACAIO){
//			aux = new Lacaio(stringGerador.nextString(), radInt(generator,1,maxMana), radInt(generator,1,maxAtaque), radInt(generator,1,maxVida), maxVida,
//					escolheHabilidade(generator));
//		}
//		else{
//			tc = escolheTipo (generator);
//			return geraCartaAleatoria(generator, maxMana, maxAtaque,  maxVida, tc);
//		}
//		
//		return aux;
//	}
//	
//	public static int radInt (Random generator, int min, int max){
//		return generator.nextInt((max - min) + 1) + min;
//	}
//	
//	public static HabilidadesLacaio escolheHabilidade (Random generator){
//		int escolha = radInt(generator,1,3);
//		
//		if(escolha == 1)
//			return HabilidadesLacaio.EXAUSTAO;
//		else if (escolha == 2)
//			return HabilidadesLacaio.INVESTIDA;
//		else
//			return HabilidadesLacaio.PROVOCAR;
//	}
//	
//	public static TipoCarta escolheTipo (Random generator){
//		int escolha = radInt(generator,1,4);
//		
//		if(escolha == 1)
//			return TipoCarta.BUFF;
//		else if (escolha == 2)
//			return TipoCarta.DANO;
//		else if (escolha == 3)
//			return TipoCarta.DANO_AREA;
//		else 
//			return TipoCarta.LACAIO;
//	}

}
