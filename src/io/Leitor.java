package io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import base.cartas.HabilidadesLacaio;
import base.cartas.Lacaio;
import base.cartas.magia.Buff;
import base.cartas.magia.Dano;
import base.cartas.magia.DanoArea;
import base.service.ILaMaSerializable;

public class Leitor {
	FileReader input ;
	
	public Leitor( String nome ) throws FileNotFoundException{
		input = new FileReader("nome");
	}
	
	public List<ILaMaSerializable> leObjeto() throws IOException {
		List<ILaMaSerializable> cards = new ArrayList();
		
		Scanner inputaux = new Scanner(input);
		
		while (inputaux.hasNext()){
			inputaux.next();
			String type = inputaux.next();
			
			if (type.equals("Lacaio")) {
				cards.add(new Lacaio(null,null,0,0,0,0, null).fromFile(inputaux));
			}
			else if (type.equals("Dano")){
				cards.add(new Dano(null,null,0,0).fromFile(inputaux));
			}
			else if (type.equals("DanoArea")){
				cards.add(new DanoArea(null,null,0,0).fromFile(inputaux));
			}
			else if (type.equals("Buff")){
				cards.add(new Buff(null,null,0,0,0).fromFile(inputaux));
			}
		}
		
		
		return cards;
	}
}
