package io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Formatter;

public class Escritor {
	FileWriter output ;
	public Escritor () throws IOException{
		output = new FileWriter(new File("Arquivo.txt"));
	}
	
	public void escreveAtributo(String nomeAtributo, String valor) throws IOException{
		output.write(nomeAtributo + " " + valor + "\n");
		output.flush();
	}
	
	public void escreveDelimObj(String nomeObj) throws IOException{
		output.write("Obj " + nomeObj+ "\n");
		output.flush();
	}
	
	public void finalizar() throws IOException{
		output.close();
	}
}
